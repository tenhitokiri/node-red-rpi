# Pruebas de uso de Node Red
    Node-RED es una herramienta de programación visual. Muestra visualmente las relaciones y funciones, y permite al usuario programar sin tener que escribir una línea. Node-RED es un editor de flujo basado en el navegador donde se puede añadir o eliminar nodos y conectarlos entre sí con el fin de hacer que se comuniquen entre ellos.
    Node-Red hace que el conectar los dispositivos de hardware, APIs y servicios en línea sea más fácil que nunca.

## Instalación en distribiciones basadas en Debian

Es necesario instalar los siguientes requisitos:

    sudo apt install build-essential git

Este script instala la última versión LTS de NodeJs, Npm y node-red como servicio

    bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)

## Paquetes

Copiar `package.json` en `./node-red` y ejecutar `npm install`

## Notas Adicionales

El pin 1 del Raspberry Pi es el que se encuentra en la parte externa, contrario a los puertos usb

![Puertos](RpiGPIOs.png)
